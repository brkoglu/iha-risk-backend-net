﻿using Microsoft.Practices.Unity;

namespace Iha.Risk.Bootstrap
{
    public static class BootstrapperFactory
    {
        public static IBootstrapper Create()
        {
            return new RiskBootstrapper(new UnityContainer());
        }
    }

}