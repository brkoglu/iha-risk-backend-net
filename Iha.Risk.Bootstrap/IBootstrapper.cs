﻿using System;

namespace Iha.Risk.Bootstrap
{
    public interface IBootstrapper
    {
        object Initialize(Type type);
        void RegisterComponents();
    }
}
