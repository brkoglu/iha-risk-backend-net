﻿using Iha.Risk.Bootstrap.Unity;
using Microsoft.Practices.Unity;

namespace Iha.Risk.Bootstrap
{
    public class RiskBootstrapper : UnityBootstrapper
    {
        public RiskBootstrapper(IUnityContainer unityContainer) : base(unityContainer)
        {

        }

        public override void RegisterComponents()
        {
            RegisterBaseComponents();
        }
    }
}
