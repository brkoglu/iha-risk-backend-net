﻿using Iha.Risk.Domain.Repository;
using Iha.Risk.Domain.Utility;
using Iha.Risk.Mapping;
using Iha.Risk.Repository.Repository;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;

namespace Iha.Risk.Bootstrap.Unity
{
    public abstract class UnityBootstrapper : IBootstrapper
    {
        private readonly string _bootstrapperName;
        protected readonly Type BootstrapperType;
        protected readonly IUnityContainer Container;

        protected UnityBootstrapper(IUnityContainer unityContainer)
        {
            if (unityContainer == null)
                throw new ArgumentNullException("unityContainer");

            Container = unityContainer;
            BootstrapperType = typeof(IBootstrapper);
            _bootstrapperName = BootstrapperType.Assembly.GetName()
                .Name.Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Last();
        }

        public object Initialize(Type type)
        {
            return Container.Resolve(type);
        }

        public abstract void RegisterComponents();

        protected IEnumerable<Type> ApplicationAssemblies()
        {
            //if (HostingEnvironment.IsHosted) // determines whether the application is web hosted or not
            return AllClasses.FromAssemblies(BuildManager.GetReferencedAssemblies().Cast<Assembly>()).Where(TypeIsFromApplication).ToList();
        }

        protected IUnityContainer RegisterBaseComponents()
        {
            return Container
                .RegisterTypes(ApplicationAssemblies(), ApplicationTypes)
                .RegisterType<IMapper, AutoMapperMapping>()
                .RegisterType<IRiskRepository, RiskRepository>()
                .RegisterType<IIdentityFactory<Guid>, GuidIdentityFactory>(new InjectionConstructor(SequentialGuidType.SequentialAtEnd));

        }

        protected IEnumerable<Type> ApplicationTypes(Type assembly)
        {
            return WithMappings.FromAllInterfaces(assembly).Where(TypeIsFromApplication).ToList();
        }

        protected virtual bool TypeIsFromApplication(Type type)
        {
            // ReSharper disable once PossibleNullReferenceException
            return type != BootstrapperType
                && type.Namespace.StartsWith(GetApplicationNamespace(), StringComparison.InvariantCultureIgnoreCase);
        }

        protected virtual string GetApplicationNamespace()
        {
            var name = Assembly.GetExecutingAssembly().GetName().Name;
            return name.Substring(0, name.IndexOf(_bootstrapperName, StringComparison.InvariantCultureIgnoreCase));
        }

    }
}
