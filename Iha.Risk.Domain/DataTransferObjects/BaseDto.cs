﻿using System;

namespace Iha.Risk.Domain.DataTransferObjects
{
    public class BaseDto
    {
        public Guid Id { get; set; }
    }
}
