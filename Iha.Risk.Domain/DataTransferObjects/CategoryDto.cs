﻿using System;
using System.Collections.Generic;

namespace Iha.Risk.Domain.DataTransferObjects
{
    public class CategoryDto : BaseDto
    {
        public string Name { get; set; }
        public string Hint { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<QuestionDto> Questions { get; set; }
    }
}
