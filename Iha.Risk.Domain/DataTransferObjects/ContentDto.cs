﻿using System;

namespace Iha.Risk.Domain.DataTransferObjects
{
    public class ContentDto : BaseDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
