﻿using System;

namespace Iha.Risk.Domain.DataTransferObjects
{
    public class ModifierDto : BaseDto
    {
        public string targetQuestion { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid OptionId { get; set; }
    }
}
