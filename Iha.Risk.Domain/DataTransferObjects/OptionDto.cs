﻿using System;
using System.Collections.Generic;

namespace Iha.Risk.Domain.DataTransferObjects
{
    public class OptionDto : BaseDto
    {
        public string Name { get; set; }
        public string Hint { get; set; }
        public string Value { get; set; }
        public decimal Factor { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid QuestionId { get; set; }
        public ICollection<ModifierDto> Modifiers { get; set; }
    }
}
