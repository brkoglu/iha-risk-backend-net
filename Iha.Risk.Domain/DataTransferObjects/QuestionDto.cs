﻿using System;
using System.Collections.Generic;

namespace Iha.Risk.Domain.DataTransferObjects
{
    public class QuestionDto : BaseDto
    {
        public string Name { get; set; }
        public string Hint { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid CategoryId { get; set; }
        public ICollection<OptionDto> Options { get; set; }
    }
}
