﻿using Iha.Risk.Domain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Iha.Risk.Domain.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        public string Hint { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Question> Questions { get; set; }

        public Question GetQuestionBy(Guid questionId)
        {
            return Questions.SingleOrDefault(r => r.Id.Equals(questionId));
        }

        protected Category()
        {
            Questions = new List<Question>();
        }

        public Category(IIdentityFactory<Guid> identityFactory)
            : base(identityFactory) { }
    }
}
