﻿using System;

namespace Iha.Risk.Domain.Entities
{
    public class Content : BaseEntity
    {
        public string Name { get; private set; }
        public string Value { get; private set; }
        public DateTime CreatedAt { get; private set; } = DateTime.Now;

        public void UpdateValue(string value)
        {
            Value = value;
        }
    }
}
