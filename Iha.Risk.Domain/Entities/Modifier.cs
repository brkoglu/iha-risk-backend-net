﻿using Iha.Risk.Domain.Utility;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Iha.Risk.Domain.Entities
{
    public class Modifier : BaseEntity
    {
        public string TargetQuestion { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid OptionId { get; set; }

        [ForeignKey("OptionId")]
        public virtual Option Option { get; set; }

        protected Modifier() { }

        public Modifier(IIdentityFactory<Guid> identityFactory)
            : base(identityFactory) { }
    }
}
