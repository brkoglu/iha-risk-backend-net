﻿using Iha.Risk.Domain.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Iha.Risk.Domain.Entities
{
    public class Option : BaseEntity
    {
        public string Name { get; set; }
        public string Hint { get; set; }
        public string Value { get; set; }
        public decimal Factor { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }

        public ICollection<Modifier> Modifiers { get; set; }

        protected Option() { }

        public Option(IIdentityFactory<Guid> identityFactory) : base(identityFactory)
        {
            Modifiers = new List<Modifier>();
        }

        public Modifier GetModifierBy(Guid modifierId)
        {
            return Modifiers.SingleOrDefault(r => r.Id.Equals(modifierId));
        }

    }
}
