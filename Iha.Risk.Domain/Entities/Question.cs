﻿using Iha.Risk.Domain.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Iha.Risk.Domain.Entities
{
    public class Question : BaseEntity
    {
        public string Name { get; set; }
        public string Hint { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Option> Options { get; set; }
        public Guid CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        protected Question() { }

        public Question(IIdentityFactory<Guid> identityFactory) : base(identityFactory)
        {
            Options = new List<Option>();
        }

        public Option GetOptionBy(Guid optionId)
        {
            return Options.SingleOrDefault(r => r.Id.Equals(optionId));
        }
    }
}
