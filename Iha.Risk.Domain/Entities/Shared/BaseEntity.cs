﻿using Iha.Risk.Domain.Utility;
using System;

namespace Iha.Risk.Domain.Entities
{
    public class BaseEntity : Entity<Guid>
    {
        protected BaseEntity() { }

        protected BaseEntity(IIdentityFactory<Guid> identityFactory)
            : base(identityFactory) { }
    }
}
