﻿using Iha.Risk.Domain.Utility;
using System;

namespace Iha.Risk.Domain.Entities
{
    public abstract class Entity<TKey> : IEquatable<Entity<TKey>>
    {
        public TKey Id { get; set; }

        protected Entity() { }

        protected Entity(IIdentityFactory<TKey> identityFactory)
        {
            if (identityFactory == null)
                throw new ArgumentNullException("identityFactory");

            Id = identityFactory.CreateIdentity();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Entity<TKey>);
        }

        public bool Equals(Entity<TKey> otherEntity)
        {
            return ReferenceEquals(otherEntity, this) && otherEntity.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
        }

        public static bool operator ==(Entity<TKey> x, Entity<TKey> y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(Entity<TKey> x, Entity<TKey> y)
        {
            return !(x == y);
        }
    }
}