﻿using Iha.Risk.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Iha.Risk.Domain.Repository
{
    public interface IRiskRepository
    {
        IEnumerable<Category> GetCategories();
        IEnumerable<Question> GetQuestions();
        IEnumerable<Option> GetOptions();
        IEnumerable<Modifier> GetModifiers();
        Category GetCategoryById(Guid id);
        Content GetContentByName(string name);
        IEnumerable<Content> GetContents();
    }
}
