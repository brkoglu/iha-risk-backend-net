﻿using Iha.Risk.Domain.Entities;
using System;

namespace Iha.Risk.Domain.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();

        void Insert<TEntity>(TEntity aggregateRoot) where TEntity : BaseEntity;
        void Delete<TEntity>(TEntity aggregateRoot) where TEntity : BaseEntity;
        void Update<TEntity>(TEntity aggregateRoot) where TEntity : BaseEntity;
        void Delete();
    }
}
