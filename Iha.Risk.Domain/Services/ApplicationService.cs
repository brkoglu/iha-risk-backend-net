﻿using Iha.Risk.Domain.DataTransferObjects;
using Iha.Risk.Domain.Entities;
using Iha.Risk.Domain.Repository;
using Iha.Risk.Domain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Iha.Risk.Domain.Services
{
    public class ApplicationService : IApplicationService
    {
        private readonly IUnitOfWork _unitOfWork;
        protected readonly IMapper _mapper;
        private readonly IRiskRepository _repository;
        private readonly IIdentityFactory<Guid> _guidIdentityFactory;

        public ApplicationService(IUnitOfWork unitOfWork, IMapper mapper, IRiskRepository repository, IIdentityFactory<Guid> guidIdentityFactory)
        {
            _mapper = mapper ?? throw new ArgumentNullException("mapper");
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException("airtsUnitOfWork");
            _repository = repository ?? throw new ArgumentNullException("repository");
            _guidIdentityFactory = guidIdentityFactory ?? throw new ArgumentNullException("guidIdentityFactory");
        }

        void IApplicationService.DeleteCategory(Guid guid)
        {
            var category = _repository.GetCategoryById(guid);
            _unitOfWork.Delete(category);
            _unitOfWork.Commit();
        }

        IEnumerable<CategoryDto> IApplicationService.GetCategories()
        {
            var categories = _repository.GetCategories();
            var questions = _repository.GetQuestions();
            var options = _repository.GetOptions();
            var modifiers = _repository.GetModifiers();

            var categoriesDto = Enumerable.Empty<CategoryDto>().ToList();
            categoriesDto.AddRange(
                categories.Select(category => GetCategoryResponse(
                    category, questions, options, modifiers)));

            return categoriesDto;
        }

        public CategoryDto GetCategory(Guid id)
        {
            var categories = _repository.GetCategories();
            var questions = _repository.GetQuestions();
            var options = _repository.GetOptions();
            var modifiers = _repository.GetModifiers();

            var selectedCategory = categories.Where(ct => ct.Id.Equals(id)).SingleOrDefault();
            var responseDto = GetCategoryResponse(selectedCategory, questions, options, modifiers);

            return responseDto;
        }

        IEnumerable<CategoryDto> IApplicationService.SetCategories(IEnumerable<CategoryDto> categories)
        {
            _unitOfWork.Delete();

            foreach (var category in categories)
            {
                var entity = _mapper.Map<CategoryDto, Category>(category);

                _unitOfWork.Insert(entity);
                _unitOfWork.Commit();
            }
            var categoriesDto = Enumerable.Empty<CategoryDto>().ToList();

            return categoriesDto;
        }

        private CategoryDto GetCategoryResponse(Category category, IEnumerable<Question> questions, IEnumerable<Option> options, IEnumerable<Modifier> modifiers)
        {
            var categoryDto = _mapper.Map<Category, CategoryDto>(category);
            GetQuestionsResponse(questions, options, modifiers, categoryDto);

            return categoryDto;
        }

        private void GetQuestionsResponse(IEnumerable<Question> questions, IEnumerable<Option> options, IEnumerable<Modifier> modifiers, CategoryDto categoryDto)
        {
            foreach (var question in questions.Where(mdg => mdg.CategoryId.Equals(categoryDto.Id)))
            {
                var questionDto = _mapper.Map<Question, QuestionDto>(question);
                GetOptionResponse(options, modifiers, questionDto);
                categoryDto.Questions.Add(questionDto);
            }
        }

        private void GetOptionResponse(IEnumerable<Option> options, IEnumerable<Modifier> modifiers, QuestionDto questionDto)
        {
            foreach (var option in options.Where(mdg => mdg.QuestionId.Equals(questionDto.Id)))
            {
                var optionDto = _mapper.Map<Option, OptionDto>(option);
                GetModifiersResponse(modifiers, optionDto);
                questionDto.Options.Add(optionDto);
            }
        }

        private void GetModifiersResponse(IEnumerable<Modifier> modifiers, OptionDto optionDto)
        {
            foreach (var modifier in modifiers.Where(mdg => mdg.OptionId.Equals(optionDto.Id)))
            {
                var modifierDto = _mapper.Map<Modifier, ModifierDto>(modifier);
                optionDto.Modifiers.Add(modifierDto);
            }
        }

        List<ContentDto> IApplicationService.GetContentsByName(IEnumerable<string> name)
        {
            var contents = new List<ContentDto>();
            foreach (var item in name)
            {
                var content = _repository.GetContentByName(item);
                if (content != null)
                {
                    contents.Add(_mapper.Map<Content, ContentDto>(content));
                }
            }
            return contents;
        }

        public void SetContents(IEnumerable<ContentDto> content)
        {
            foreach (var item in content)
            {
                SetContent(item);
            }
        }
        private void SetContent(ContentDto content)
        {
            if (content != null && content.Name != null)
            {
                var entity = _mapper.Map<ContentDto, Content>(content);
                var existingItem = _repository.GetContentByName(entity.Name);
                if (existingItem != null)
                {
                    existingItem.UpdateValue(content.Value);
                    _unitOfWork.Update(existingItem);
                }
                else
                {
                    _unitOfWork.Insert(entity);
                }
                _unitOfWork.Commit();
            }
        }

        public void DeleteContent(ContentDto content)
        {
            throw new NotImplementedException();
        }

        List<ContentDto> IApplicationService.GetContents()
        {
            var contents = new List<ContentDto>();
            var items = _repository.GetContents();

            foreach (var item in items)
            {
                contents.Add(_mapper.Map<Content, ContentDto>(item));
            }
            return contents;
        }


    }
}
