﻿using Iha.Risk.Domain.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace Iha.Risk.Domain.Services
{
    public interface IApplicationService
    {
        IEnumerable<CategoryDto> GetCategories();
        CategoryDto GetCategory(Guid id);
        IEnumerable<CategoryDto> SetCategories(IEnumerable<CategoryDto> categories);
        void DeleteCategory(Guid guid);
        List<ContentDto> GetContentsByName(IEnumerable<string> names);
        List<ContentDto> GetContents();
        void SetContents(IEnumerable<ContentDto> content);
        void DeleteContent(ContentDto content);
    }
}
