﻿namespace Iha.Risk.Domain.Utility
{
    public interface IIdentityFactory<out TKey>
    {
        TKey CreateIdentity();
    }
}
