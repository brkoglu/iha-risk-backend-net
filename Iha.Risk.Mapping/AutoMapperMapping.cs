﻿using AutoMapper;
using AutoMapper.Mappers;
using Iha.Risk.Domain.Utility;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Iha.Risk.Mapping
{
    [SuppressMessage("Microsoft.Design", "CA1001", Justification = "Currently there is no other way - Ivan")]
    public sealed class AutoMapperMapping : IMapper
    {
        private readonly IMappingEngine _mappingEngine;

        public AutoMapperMapping()
        {
            var configurationStore = new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
            AutoMapperProfiles.Get().ToList().ForEach(configurationStore.AddProfile);
            //configurationStore.AddProfile(profile.Get());
            _mappingEngine = new MappingEngine(configurationStore);
        }

        TDestination IMapper.Map<TSource, TDestination>(TSource source)
        {
            return _mappingEngine.Map<TSource, TDestination>(source);
        }

        TDestination IMapper.Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return _mappingEngine.Map(source, destination);
        }

        IEnumerable<TDestination> IMapper.Map<TSource, TDestination>(IEnumerable<TSource> source)
        {
            return _mappingEngine.Map<IEnumerable<TSource>, IEnumerable<TDestination>>(source);
        }

    }
}
