﻿using AutoMapper;
using Iha.Risk.Domain.DataTransferObjects;
using Iha.Risk.Domain.Entities;
using System;

namespace Iha.Risk.Mapping
{
    public class BaseProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<Question, QuestionDto>().ReverseMap();
            CreateMap<Option, OptionDto>().ReverseMap();
            CreateMap<Modifier, ModifierDto>().ReverseMap();
            CreateMap<Content, ContentDto>().ReverseMap()
                //.ForMember(x => x.Id, opt => opt.MapFrom(o => Guid.NewGuid()))
                .ForMember(x => x.CreatedAt, opt => opt.MapFrom(o => DateTime.Now));
        }
    }
}
