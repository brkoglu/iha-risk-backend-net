﻿using Iha.Risk.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Iha.Risk.Repository
{
    internal class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            HasMany(s => s.Questions)
            .WithRequired(s => s.Category)
            .HasForeignKey(s => s.CategoryId);
        }
    }
}