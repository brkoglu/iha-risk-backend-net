﻿using Iha.Risk.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Iha.Risk.Repository
{
    internal class OptionConfiguration : EntityTypeConfiguration<Option>
    {
        public OptionConfiguration()
        {
            HasRequired(q => q.Question)
                .WithMany(q => q.Options)
                .HasForeignKey(q => q.QuestionId);

            Property(x => x.Factor).HasPrecision(16, 8);
        }
    }
}