﻿using Iha.Risk.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Iha.Risk.Repository
{
    internal class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            HasRequired(q => q.Category)
            .WithMany(q => q.Questions)
            .HasForeignKey(q => q.CategoryId);
        }
    }
}