﻿using Iha.Risk.Domain.Entities;
using System.Linq;

namespace Iha.Risk.Repository
{
    public interface IContext
    {
        IQueryable<TBaseEntity> GetContext<TBaseEntity>() where TBaseEntity : BaseEntity;
    }
}
