namespace Iha.Risk.Repository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Hint = c.String(),
                    CreatedAt = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Questions",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Hint = c.String(),
                    CreatedAt = c.DateTime(nullable: false),
                    CategoryId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);

            CreateTable(
                "dbo.Options",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Hint = c.String(),
                    Value = c.String(),
                    Factor = c.Decimal(nullable: false, precision: 18, scale: 2),
                    CreatedAt = c.DateTime(nullable: false),
                    QuestionId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);

            CreateTable(
                "dbo.Modifiers",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    TargetQuestion = c.String(),
                    CreatedAt = c.DateTime(nullable: false),
                    OptionId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Options", t => t.OptionId, cascadeDelete: true)
                .Index(t => t.OptionId);

            CreateTable(
                "dbo.Contents",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Value = c.String(),
                    CreatedAt = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Questions", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Options", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Modifiers", "OptionId", "dbo.Options");
            DropIndex("dbo.Modifiers", new[] { "OptionId" });
            DropIndex("dbo.Options", new[] { "QuestionId" });
            DropIndex("dbo.Questions", new[] { "CategoryId" });
            DropTable("dbo.Contents");
            DropTable("dbo.Modifiers");
            DropTable("dbo.Options");
            DropTable("dbo.Questions");
            DropTable("dbo.Categories");
        }
    }
}
