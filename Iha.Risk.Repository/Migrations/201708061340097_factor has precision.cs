namespace Iha.Risk.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class factorhasprecision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Options", "Factor", c => c.Decimal(nullable: false, precision: 16, scale: 8));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Options", "Factor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
