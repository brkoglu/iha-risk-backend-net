﻿using Iha.Risk.Domain.Entities;
using Iha.Risk.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Iha.Risk.Repository.Repository
{
    public class RiskRepository : IRiskRepository
    {
        private readonly IContext _context;

        public RiskRepository(IContext context)
        {
            _context = context ?? throw new ArgumentNullException("context");
        }

        IEnumerable<Category> IRiskRepository.GetCategories()
        {
            return _context.GetContext<Category>().AsNoTracking().ToList();
        }

        IEnumerable<Question> IRiskRepository.GetQuestions()
        {
            return _context.GetContext<Question>().AsNoTracking().ToList();
        }

        IEnumerable<Option> IRiskRepository.GetOptions()
        {
            return _context.GetContext<Option>().AsNoTracking().ToList();
        }

        IEnumerable<Modifier> IRiskRepository.GetModifiers()
        {
            return _context.GetContext<Modifier>().AsNoTracking().ToList();
        }

        Category IRiskRepository.GetCategoryById(Guid id)
        {
            return _context.GetContext<Category>().AsNoTracking().SingleOrDefault(q => q.Id.Equals(id));
        }

        Content IRiskRepository.GetContentByName(string name)
        {
            return _context.GetContext<Content>().AsNoTracking().SingleOrDefault(q => q.Name.Equals(name));
        }

        public IEnumerable<Content> GetContents()
        {
            return _context.GetContext<Content>().AsNoTracking().ToList();
        }
    }
}
