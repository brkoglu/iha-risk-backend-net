﻿using Iha.Risk.Domain.Entities;
using Iha.Risk.Domain.Repository;
using Iha.Risk.Repository.Configuration;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace Iha.Risk.Repository
{
    public class UnitOfWork : DbContext, IUnitOfWork, IContext
    {
        public DbSet<Category> CategoryData { get; private set; }
        public DbSet<Option> OptionData { get; private set; }
        public DbSet<Question> QuestionData { get; private set; }
        public DbSet<Modifier> ModifierData { get; private set; }
        public DbSet<Content> ContentData { get; private set; }

        public UnitOfWork() : base("RiskConnection")
        {
            var ensureDllIsCopied = SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new OptionConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new ModifierConfiguration());
            modelBuilder.Configurations.Add(new ContentConfiguration());
        }

        IQueryable<TBaseEntity> IContext.GetContext<TBaseEntity>()
        {
            return Set<TBaseEntity>();
        }

        void IUnitOfWork.Commit()
        {
            SaveChanges();
        }

        void IUnitOfWork.Insert<TEntity>(TEntity entity)
        {
            Set<TEntity>().Add(entity);
        }

        void IUnitOfWork.Update<TEntity>(TEntity entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        void IUnitOfWork.Delete<TEntity>(TEntity entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public void Delete()
        {
            this.Database.ExecuteSqlCommand("DELETE FROM [Categories]");
        }
    }
}
