﻿using Iha.Risk.Bootstrap;
using Iha.Risk.Web.ControllerActivators;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace Iha.Risk.Web.App_Start
{
    public class DIContainerConfig
    {
        public static void SetDependencyInjectionContainer()
        {
            var bootstrapper = BootstrapperFactory.Create();
            bootstrapper.RegisterComponents();

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new BootstrapperHttpControllerActivator(bootstrapper)); // Web API
            //ControllerBuilder.Current.SetControllerFactory(new BootstrapperControllerFactory(bootstrapper)); // MVC
        }
    }
}