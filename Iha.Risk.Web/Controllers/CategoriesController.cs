﻿using Iha.Risk.Domain.DataTransferObjects;
using Iha.Risk.Domain.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Iha.Risk.Web.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly IApplicationService _applicationService;

        public CategoriesController(IApplicationService applicationService)
        {
            _applicationService = applicationService ?? throw new ArgumentNullException("applicationService");
        }

        // GET api/values
        public IEnumerable<CategoryDto> Get()
        {
            return _applicationService.GetCategories();
        }

        // GET api/values/5
        public CategoryDto Get(string id)
        {
            return _applicationService.GetCategory(Guid.Parse(id));
        }

        // POST api/values
        public void Post([FromBody]object value)
        {
            var category = JsonConvert.DeserializeObject<List<CategoryDto>>(value.ToString());
            _applicationService.SetCategories(category);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(string id)
        {
            _applicationService.DeleteCategory(Guid.Parse(id));
        }
    }
}
