﻿using Iha.Risk.Domain.DataTransferObjects;
using Iha.Risk.Domain.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Iha.Risk.Web.Controllers
{
    public class ContentController : ApiController
    {
        private readonly IApplicationService _applicationService;

        public ContentController(IApplicationService applicationService)
        {
            _applicationService = applicationService ?? throw new ArgumentNullException("applicationService");
        }

        // GET api/values
        public List<ContentDto> Get([FromUri] List<string> content)
        {
            if (content.Any())
                return _applicationService.GetContentsByName(content);

            return _applicationService.GetContents();
        }

        // POST api/values
        public void Post([FromBody]object value)
        {
            var content = JsonConvert.DeserializeObject<IEnumerable<ContentDto>>(value.ToString());
            _applicationService.SetContents(content);

        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(string id)
        {
            _applicationService.DeleteCategory(Guid.Parse(id));
        }
    }
}
