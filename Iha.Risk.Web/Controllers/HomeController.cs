﻿using System.Web.Mvc;

namespace Iha.Risk.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "IHA Risk Form Backend";

            return View();
        }
    }
}
